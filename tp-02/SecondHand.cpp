#include "SecondHand.h"

int SecondHand::get_seconds()
{
    return _seconds;
}

void SecondHand::advance()
{
    _seconds++;
    if (_seconds % 60 == 0)
    {
        _seconds = 0;
        _min_hand.advance();
    }
}