#include <iostream>
#include <vector>
#include "Histogram.h"

// std::vector<unsigned int> count_lower(const std::string &s)
// {
//     std::vector<unsigned int> alpha_count;
//     for (int i = 0; i < 26; i++)
//     {
//         auto l = 'a' + i;
//         auto nb = 0;
//         for (auto c : s)
//         {
//             if (c == l)
//             {
//                 nb++;
//             }
//         }
//         alpha_count.emplace_back(nb);
//     }
//     return alpha_count;
// }

// void display_lower_occ(const std::vector<unsigned int> &array)
// {
//     for (int i = 0; i < 26; i++)
//     {
//         char l = 'a' + i;
//         std::cout << l << ":" << array[i] << std::endl;
//     }
// }

std::string read_input(void)
{
    std::string s;
    std::cin >> s;
    return s;
}

void concat(std::string &s1, const std::string &s2)
{
    s1 += s2;
}

int main()
{
    Histogram h1, h2;
    Histogram h3{Histogram{h1}};
    Histogram histo;
    std::string all_s;
    while (true)
    {
        auto s = read_input();
        if (s == "QUIT")
        {
            break;
        }
        concat(all_s, s);
    }

    histo.analyze(all_s);
    histo.print();
    std::cout << all_s << std::endl;

    Histogram histo2 = histo;

    return 0;
}