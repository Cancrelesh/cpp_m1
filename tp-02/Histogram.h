#pragma once

#include <vector>
#include <utility>
#include <iostream>

class Histogram
{
public:
    void analyze(const std::string &); // analyse la string passée en paramètre
    void print() const;                // affiche l'histogramme

    Histogram() = default;
    Histogram(const Histogram &histo);

private:
    std::vector<unsigned int> _count = std::vector<unsigned int>(26, 0); // tableau contenant le nombre d'occurrences de chaque lettre entre 'a' et 'z'
};