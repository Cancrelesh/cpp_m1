#include "Histogram.h"

void Histogram::analyze(const std::string &s)
{
    for (int i = 0; i < (int)_count.size(); i++)
    {
        auto l = 'a' + i;
        // auto nb = 0;
        for (auto c : s)
        {
            if (c == l)
            {
                _count[i] += 1;
            }
        }
        // _count.emplace_back(nb);
    }
}

void Histogram::print() const
{
    for (int i = 0; i < (int)_count.size(); i++)
    {
        char l = 'a' + i;
        std::cout << l << ":" << _count[i] << std::endl;
    }
}

Histogram::Histogram(const Histogram &histo)
    : _count{histo._count}
{
    std::cout << "copy" << std::endl;
}