
#pragma once

#include <vector>
#include <utility>
#include <ostream>

using Vertex = std::pair<int, int>;

class Polygon
{
    friend std::ostream &operator<<(std::ostream &stream, const Polygon &polygon);

private:
    std::vector<Vertex> _vertices;

public:
    void add_vertex(int x, int y);
    const Vertex &get_vertex(size_t position);
};
