#include "Rectangle.h"

#include <iostream>

int main()
{
    Rectangle rect(2.f, 4.f);
    std::cout << rect << std::endl;
    rect.scale(3);

    Rectangle square(2.5f);
    std::cout << square << std::endl;
    Rectangle::set_default_size(3.f);
    Rectangle s1;
    Rectangle s2;
    std::cout << s1 << std::endl;
    std::cout << s2 << std::endl;
    Rectangle::set_default_size(5.f);
    Rectangle s3;
    Rectangle s4;
    Rectangle s5;
    std::cout << s3 << std::endl;
    std::cout << s4 << std::endl;
    std::cout << s5 << std::endl;

    return 0;
}
