#include <iostream>
#include <vector>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cerr << "you must put an argument" << std::endl;
        return -1;
    }
    // int array[] = {0, 1, 2, 3};
    // int array[50] = {}; // nad avait raison, {} initialise bien avec des 0, pas besoin de {0}

    // for (int val : array)
    // {
    //     std::cout << val << std::endl;
    // }

    // std::cout << std::endl;

    std::vector<int> array;

    // for (int i = 0; i < 50; i++)
    // {
    //     array[i] = i + 1;
    // }
    array.emplace_back(8);
    array.emplace_back(5);
    array.emplace_back(-3);

    // for (int i = 0; i < 4; ++i)
    // {
    //     std::cout << array[i] << std::endl;
    // }

    for (int val : array)
    {
        std::cout << val << std::endl;
    }

    for (int i = 1; i < argc; i++)
    {
        std::cout << argv[i] << std::endl;
    }

    return 0;
}
