#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

bool parse_params(const int &argc, char **argv, string &dict_path, vector<string> &word, vector<string> &translation, vector<string> &sentence);
vector<pair<string, string>> open_dictionary(const string &path);
void save_dictionary(const string &path, const vector<pair<string, string>> &dict);
void translate(const vector<string> &sentence, const vector<pair<string, string>> &dict);
bool dict_contains(const vector<pair<string, string>> &dict, const string &word, const string &traduction);

int main(int argc, char **argv)
{
    string dict_path;
    vector<string> word, translation, sentence;

    if (!parse_params(argc, argv, dict_path, word, translation, sentence))
    {
        return -1;
    }

    vector<pair<string, string>> dict;
    auto length_dict_path = dict_path.size();

    if (length_dict_path > 0)
    {
        dict = open_dictionary(dict_path);
    }

    if (word.size() > 0 && translation.size() > 0)
    {
        for (int i = 0; i < (int)word.size() && i < (int)translation.size(); i++)
        {
            if (!dict_contains(dict, word[i], translation[i]))
            {
                dict.emplace_back(word[i], translation[i]);
            }
        }

        if (length_dict_path > 0)
        {
            save_dictionary(dict_path, dict);
        }
    }

    if (sentence.size() > 0)
    {
        translate(sentence, dict);
    }

    return 0;
}

bool parse_params(const int &argc, char **argv, string &dict_path, vector<string> &word, vector<string> &translation, vector<string> &sentence)
{
    for (auto i = 1; i < argc; ++i)
    {
        const std::string option = argv[i];

        if (option == "-d" && (i + 1) < argc)
        {
            dict_path = argv[++i];
        }
        else if (option == "-a" && (i + 2) < argc)
        {
            word.emplace_back(argv[++i]);
            translation.emplace_back(argv[++i]);
        }
        else
        {
            sentence.emplace_back(argv[i]);
        }
    }

    if (dict_path.empty())
    {
        cerr << "No dictionary path was provided." << endl;
        return false;
    }

    return true;
}

vector<pair<string, string>> open_dictionary(const string &path)
{
    vector<pair<string, string>> dict;

    fstream file;
    file.open(path, ios_base::in);

    if (file.is_open())
    {
        int i = 0;
        while (!file.eof())
        {
            string word;
            file >> word;

            string translation;
            file >> translation;

            dict.emplace_back(pair{word, translation});
            i++;
        }
        file.close();
    }

    return dict;
}

void save_dictionary(const string &path, const vector<pair<string, string>> &dict)
{
    fstream file{path, ios_base::out};

    for (auto word_translation : dict)
    {
        file << word_translation.first << " " << word_translation.second << std::endl;
    }
    file.close();
}

void translate(const vector<string> &sentence, const vector<pair<string, string>> &dict)
{
    for (auto word : sentence)
    {
        for (int i = 0; i < (int)dict.size(); i++)
        {
            if (word == dict[i].first)
            {
                cout << dict[i].second << " ";
                break;
            }
            if (i == (int)dict.size() - 1)
            {
                cout << word << " ";
            }
        }
    }
}

bool dict_contains(const vector<pair<string, string>> &dict, const string &word, const string &traduction)
{
    for (auto wt : dict)
    {
        if (wt.first == word && wt.second == traduction)
        {
            return true;
        }
    }
    return false;
}
