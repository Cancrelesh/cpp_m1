# TP1 - Introduction au C++


## Exercice 1 - Compilation et exécution

1. Quels sont les avantages et désavantages d'un langage dit "*compilé*" (C, C++, Pascal) ou "*semi-compilé*" (Java) comparé à un langage dit "*interpreté*" (Python, PHP, Javascript, etc) ?

- Avantages :<br/>
Ça va plus vite.

- Désavantages :<br/>
On doit recompiler le programme pour chaque machine pour avoir un executable fonctionnel.


2. Quelle est la différence entre une erreur de compilation et une erreur d'exécution ? (à quel moment se produisent-elles ? dans quelles circonstances ? comment les identifier ? comment les corriger ? ...)

- Erreur de compilation : <br/>Une erreur de compilation empêche la génération du code exécutable à partir du code source. Il peut s’agir d’une erreur de syntaxe ou de l’utilisation d’une librairie qui n’est pas accessible au compilateur.
L'erreur se passe au moment de la compilation.
Elles peuvent être révélés par des Error ou de Warning, ce qui nous permet de les corriger plus facilement.

- Erreur d'execution :
<br/>Une erreur d’exécution se produit, comme son nom l’indique, au moment de l’exécution du programme. Cette erreur peut être liée à une erreur de programmation, comme un défaut de vérification des arguments d’une fonction, ou faire partie du fonctionnement normal du programme (erreur affichée à l’utilisateur en cas de saisie erronée par exemple).
Pour les corriger suffit d'avoir des bonnes notions d'algo et de corriger son code.


3. Que signifie en pratique l'expression "*undefined behavior*" (UB) ? Peut-on compiler un programme contenant du code classifié UB par le standard ? Si oui, que peut-il se produire au moment de son exécution ?

- C'est ce qui n'est pas supporté par le standard. On peut compiler ce programme mais rien n'est sûr pour le resultat attendu car ce n'est pas quelque chose de supporté par le compilateur.

## Exercice 2 - Primitives et initialisation

En C++, il existe plein de syntaxes différentes pour initialiser une variable de type primitif (int, char, bool, ...) :

```cpp
int v;       // pas d'initialisation            -> valeur indéfinie
int v = 2;   // initialisation avec '= value'   -> value
int v {};    // initialisation avec '{}'        -> tous les bits à 0
int v { 2 }; // initialisation avec '{ value }' -> value
int v(2);    // initialisation avec '(value)'   -> value
```

1. Parmi les lignes suivantes, lesquelles déclenchent des erreurs de compilation ? Quelles sont ces erreurs et comment les corriger ?\
Mêmes questions en ajoutant l'option `-Werror` à la compilation.\
Vous pouvez utiliser [CompilerExplorer](https://www.godbolt.org/z/rPPoro) pour tester la compilation de petits snippets de code.

```cpp
short       s0;
const short s1; // error: uninitialized 'const s1'[-fpermissive] const short s1;
// pour corriger il faut attribuer une valeur à s1

const int i1 = 2;

bool b2{false};
bool b3{i1}; // error: narrowing conversion of '2' from 'int' to 'bool' [-Wnarrowing] bool b3{i1};
// Il y un conflit en tre le type du tableau 'bool' et la variable d'initialisation de celui-ci : i1 qui est de type 'int'.
// Pour corriger, tout dépend de ce que l'on veut faire dans le programme:
// - si manipulation d'un tableau de bool : int i1 -> bool i1
// - si manipulation d'un tableau de int : bool b3 -> int b3
bool b4;

unsigned       u5{0x10};
unsigned short us6 = -10;
unsigned long  ul7{b3 + u5 + us6};

char c8{"a"}; // error: invalid conversion from 'const char*' to 'char' [-fpermissive] char c8{"a"};
// il y a un problème pour convertir le type de "a" (const char*) vers le type "char";
// pour régler le problème autant mettre directement 'a' au lieu de "a".
char c9 = -10;

double       d10{i1};
double&      d11{d10};
double&      d12; // error: 'd12' declared as reference but not initialized double&      d12;
// d12 ne référence aucune valeur.
// Pour corriger il faut assigner une valeur à d12.
const double d13{.0f};

int        i14 = i1;
int&       i15 = i1; // error: binding reference of type 'int&' to 'const int' discards qualifiers int&       i15 = i1;
// On ne peut pas référencer un const T par un non-const T.
// Pour corriger on peut faire const int& i15 = i1.
int&       i16 = b2; // error: cannot bind non-const lvalue reference of type 'int&' to an rvalue of type 'int' int&       i16 = b2;
// - d'abord le compilateur essai de convertir le bool en int.
// - ce int est une variable temporaire voilà pourquoi il ne peut pas être référencé.
// pour corriger on peut d'abord enregistrer ce int temporaire dans une nouvelle variable puis ensuite le référencer par i16.
const int& i17{i14};
```
- `-Werror` ne change rien.

2. Pouvez-vous donner la valeur de `s0` ? De `ul7` ?
- s0 = on sait pas
- ul7 = b3 + u5 + us6 = un truc cassé + 16 + (-10) = un truc cassé + 6 = un truc cassé.


## Exercice 3 - Les fonctions et leurs paramètres

1. Remplacez les `XX` par les bons types, de manière à ce que le programme compile et affiche `42`.

```cpp
#include <iostream>

int add(int a, int b) {
  return a + b;
}

void add_to(int& a, int b) {
  a += b;
}

void another_add_to(int& a, int b) {
  a += b;
}

int main() {
  const int x{10}; // x = 10
  int y = add(x, x); // y = 20
  another_add_to(y, y); // y = 40
  add_to(y, 2); // y = 42
  std::cout << y << std::endl;

  return 0;
}
```

2. En C++, vous pouvez passer vos paramètres par valeur, par référence et par référence constante.
Quelles sont les différences entre ces différentes méthodes de passage ?
Dans quels contextes est-il préférable de passer par valeur ? Par référence ? Et par référence constante ?

- passage par valeur :
<br/> On copie la variable localement dans le scope de la fonction. Elle est modifiable.
<br/> La variable en dehors de la fonction n'est pas modifiée sauf si on la return.
- passage par reference :
<br/> On utilise une référence vers la variable qui est en dehors de la fonction. S'il y a une modifiaction sur la référence dans la fonction, alors la variable en dehors de la fonction est aussi impactée.
- passage par reference constante :
<br/> On peut utiliser la variable défini en dehors la fonction grâce à sa référence mais elle n'est pas modifiable car constante.

3. Modifiez les signatures des fonctions suivantes de manière à ce que le passage de paramètres soit le plus efficace et sécurisé possible.
Vous pouvez vous aidez des commentaires pour comprendre comment les fonctions utilisent leurs paramètres.
```cpp
// Return the number of occurrences of 'a' found in string 's'.
int count_a_occurrences(const std::string& s);

// Update function of a rendering program.
// - dt (delta time) is read by the function to know the time elapsed since the last frame.
// - errors is a string filled by the function to indicate what errors have occured.
void update_loop(const float dt, std::string& errors_out);

// Return whether all numbers in 'values' are positive.
// If there are negative values in it, fill the array 'negative_indices_out' with the indices
// of these values and set its size in 'negative_count_out'.
// ex: auto res = are_all_positive({ 1, -2, 3, -4 }, negative_indices, negative_count);
//    -> res is false, since not all values are positive
//    -> negative_indices contains { 1, 3 } because values[1] = -2 and values[3] = -4
//    -> negative_count is 2
bool are_all_positives(const std::vector<int>& values, std::vector<int>& negative_indices_out);

// Concatenate 'str1' and 'str2' and return the result.
// The input parameters are not modified by the function.
std::string concatenate(const std::string& str1, const cstd::stringhar& str2);
```


## Exercice 4 - `std::string` et `std::vector`

1. Ecrivez un programme qui utilise `std::cin` pour lire une variable de type `std::string`, puis calcule et affiche pour chaque lettre miniscule ('a', 'b', ..., 'z') le nombre des fois où elle apparaît.\
Le comptage des lettres se fera dans une fonction `count_lower` prenant une chaîne de caractères en paramètre et renvoyant un `std::vector<unsigned int>`. La première case du tableau contiendra le nombre d'occurences de 'a', la seconde de 'b', etc.\
Vous afficherez ce tableau dans une seconde fonction `display_lower_occ`.\
Essayez d'utiliser les signatures qui rendront votre programme le plus efficace possible.

2. Modifiez le programme pour que l'utilisateur puisse entrer plusieurs chaînes de caractères, jusqu'à ce qu'il entre la chaîne "QUIT".\
Vous afficherez en plus des occurrences la concaténation de toutes les chaînes de caractères renseignées par l'utilisateur.


## Exercice 5 - Traducteur malade

Dans la vie, on se retrouve souvent face à du code tout pourri qu'il faut débugguer. Afin de vous préparer psychologiquement aux tourments de votre futur métier de programmeur, vous allez devoir corriger le code du programme que vous trouverez dans le dossier `tp-01` du dépôt https://github.com/Laefy/CPP_Learning_Code/.

---

Ce programme consiste en un traducteur, censé s'utiliser comme suit :
```b
# Ajoute une nouvelle traduction au dictionnaire dict.txt (il est créé s'il n'existe pas).
./translator -d path/to/dict.txt -a chat cat

# Utilise le dictionnaire dict.txt pour traduire la phrase "le chat est beau"
./translator -d path/to/dict.txt le chat est beau
```

Voici le format attendu pour le fichier dict.txt :
```b
bonjour hello
cheval horse
tu you
```
---

1. Pour commencer, essayez de faire en sorte que le programme compile.\
Un indice : si seulement on pouvait bannir les chaînes de caractères de type char*.

2. Une fois que le programme compile, essayez de le lancer, afin de constater que le programme est bourré de bug.\
Placez un breakpoint sur la première ligne du `main`, lancez le programme en mode debug et itérez d'instruction en instruction, tout en inspectant le contenu des variables.\
Cela vous permettra d'identifier d'où viennent les problèmes pour réussir à les corriger.

> Dans VSCode, vous pouvez placer des breakpoints avec F9, lancer un programme en mode debug via F5 (il faut modifier le fichier launch.json pour passer des arguments au programme), exécuter l'instruction courante avec F10, entrer dans un appel de fonction avec F11 et en sortir avec Shift+F11. Afin de voir le contenu des variables, ouvrez le panneau d'exécution (`View > Run`) et regardez dans la section `Variables`.

> Voici la liste des types de problèmes que trouverez dans le programme :
>- passage par valeur au lieu de référence,
>- mauvais usage de `std::vector`,
>- condition inversée ou au mauvais endroit.

3. Si vous parvenez à corriger tous les bugs, vous pouvez faire une dernière passe sur le programme afin de remplacer les passages par valeur coûteux par des passages par const-ref, et ajouter les `const` sur toutes les variables qui ne sont pas modifiées après leur initialisation.   

---

Céline Noël, Stéphane Vialette, Mathias Weller  
C++ Master 1    
2020 - 2021
