#pragma once

class Driver
{
private:
    bool _licence     = false;
    bool _air_licence = false;

public:
    Driver();
    ~Driver();
    void pass_car_licence_exam();
    bool has_car_licence() const;
    void pass_air_licence_exam();
    bool has_air_licence() const;
};

Driver::Driver()
{}

Driver::~Driver()
{}

void Driver::pass_car_licence_exam()
{
    _licence = true;
}

bool Driver::has_car_licence() const
{
    return _licence;
}

void Driver::pass_air_licence_exam()
{
    _air_licence = true;
}

bool Driver::has_air_licence() const
{
    return _air_licence;
}