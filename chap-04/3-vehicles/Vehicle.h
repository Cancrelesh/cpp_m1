#pragma once

#include "Driver.h"

#include <iostream>

class Vehicle
{
public:
    virtual ~Vehicle() {}

    Vehicle(const Driver& driver)
        : _driver { driver }
    {}

    virtual unsigned int drive() const = 0;
    // {
    //     std::cerr << "Not implemented" << std::endl;
    //     return 0u;
    // }

private:
protected:
    const Driver& _driver;
};
