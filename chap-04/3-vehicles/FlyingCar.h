#pragma once

#include "Car.h"

class FlyingCar : public Car
{
private:
public:
    FlyingCar(const Driver& driver, unsigned int speed);
    ~FlyingCar();
    unsigned int drive() const override;
};

FlyingCar::FlyingCar(const Driver& driver, unsigned int speed)
    : Car { driver, speed }
{}

FlyingCar::~FlyingCar()
{}

unsigned int FlyingCar::drive() const
{

    if (_driver.has_air_licence())
    {
        std::cout << "Vrooooom in the SKYYYYYYYYYY!" << std::endl;
        return _speed * 10;
    }
    else
    {
        return Car::drive();
    }
}