#pragma once

#include "Animal.h"

#include <iostream>

class Chicken : public Animal
{
public:
    ~Chicken() override;
    void sing(char next_char) const override { std::cout << "Cotcotcotcodet" << next_char; }
};

Chicken::~Chicken()
{
    std::cout << "CotCoooooooooooooot!" << std::endl;
}
