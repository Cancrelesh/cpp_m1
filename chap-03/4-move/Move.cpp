#include <iostream>
#include <memory>
#include <utility>
#include <vector>

void display(const std::string &variable_name, const std::unique_ptr<int> &variable)
{
    if (variable)
    {
        std::cout << variable_name << " contains " << *variable << std::endl;
    }
    else
    {
        std::cout << variable_name << " is empty" << std::endl;
    }
}

std::unique_ptr<int> passthrough(std::unique_ptr<int> ptr)
{
    std::cout << "Before move in passthrough" << std::endl;
    display("ptr", ptr);
    std::cout << "--------------------------" << std::endl;

    std::unique_ptr<int> var_local = std::move(ptr);
    std::cout << "After move in passthrough" << std::endl;
    display("moved_ptr", var_local);
    display("ptr", ptr);
    std::cout << "--------------------------" << std::endl;

    return var_local;
}

int main()
{
    // std::vector<std::string> many_ints;

    // auto value = "will I move?";

    // std::cout << "value is " << value << std::endl;

    // many_ints.emplace_back(std::move(value));

    // std::cout << "value is " << value << std::endl;

    // std::cout << "many_ints[0] is " << many_ints[0] << std::endl;

    std::unique_ptr<int> i1, i2;
    i1 = std::make_unique<int>(15);
    i2 = std::make_unique<int>(65);

    std::cout << "Before passthrough call" << std::endl;
    display("i1", i1);
    display("i2", i2);
    std::cout << "--------------------------" << std::endl;

    i2 = passthrough(std::move(i1));

    std::cout << "After passthrough call" << std::endl;
    display("i1", i1);
    display("i2", i2);
    std::cout << "--------------------------" << std::endl;
    return 0;
}